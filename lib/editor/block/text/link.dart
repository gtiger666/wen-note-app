import 'package:note/editor/block/text/text.dart';

class BlockLink{
  WenTextElement textElement;
  int textOffset;

  BlockLink({
    required this.textElement,
    required this.textOffset,
  });
}